#include "header/write_can_frame.hpp"

int conv_param_name_i(std::string name)
{
    if(name == "param1")
        return PARAM1;
    else if(name == "param2")
        return PARAM2;
    else if(name == "param3")
        return PARAM3;
    else if(name == "param4")
        return PARAM4;
    else if(name == "param5")
        return PARAM5;
    
}

void write_can_frame(int fd, param_struct *param_, int opc)
{
    int n;
    int idp,type;
    int value;
    struct can_frame frame;
    frame.can_id = MODULE_CAN_ID | opc;
    frame.can_dlc = 8;
    type = (uint8_t) param_->type;
    idp = (uint16_t) conv_param_name_i(param_->name);
    switch(type)
    {
        case IS_INT:
            value = (uint32_t)*((int *)param_->value);
            break;
        case IS_FLOAT:
            value = (uint32_t)*((float *)param_->value);
            break;
        case IS_DOUBLE:
            value = (uint32_t)*((double *)param_->value);
            break;
        /*case IS_CHAR:
            param_->value = malloc(sizeof(char)*value.length());
            strcpy(((char *)param_->value),buffer[VALUE_b].c_str());
            break;*/
        default:
            std::cerr << "Type is not allowed yet.." << std::endl;
            break;
    }
    //std::cout << "opc: "<< opc << std::endl << "idp: " << std::hex << idp << std::endl << "type: " << std::hex << type << std::endl << "value: "; 
    frame.data[0] = (idp >> 8) & 0xff;
    frame.data[1] = idp & 0xff;
    frame.data[2] = type;
    frame.data[3] = 0x00;
    *(uint32_t *)((frame.data)+4) = ((value>>24)&0xff) | ((value<<8)&0xff0000) | ((value>>8)&0xff00) | ((value<<24)&0xff000000); // byte 0 to byte 3;
    n = write(fd,&frame,sizeof(frame));
    if(n < 0)
        perror("Unable to write can frame..");
}