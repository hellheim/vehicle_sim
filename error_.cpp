#include "header/error_.hpp"
#include <string>
#include <iostream>
#include <stdlib.h>

void error_(std::string msg)
{
    std::cerr << msg << std::endl;
    exit(-1);
}