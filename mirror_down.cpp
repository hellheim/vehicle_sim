#include "header/mirror_down.hpp"

extern int mirrorDC, fd;

void mirror_down(void)
{  
    param_struct param;
    std::string entry;
    mirrorDC -= mirrorAdd;
    if(mirrorDC < minROT)
        mirrorDC = minROT;
    pwmWrite(mirrorCommand,mirrorDC);
    sleep(1);
    pwmWrite(mirrorCommand,0);
    entry = "0:param3:"+std::to_string(mirrorDC);
    parse_entry(entry,&param);
    write_can_frame(fd,&param,OPC_READ);
}