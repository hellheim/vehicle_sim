#include "header/sig_handler.hpp"

extern int fd_sig_handler;
extern int usr_id;

void sig_handler(int signum)
{
    digitalWrite(radioChannel0,LOW);
    digitalWrite(radioChannel1,LOW);
    digitalWrite(radioChannel2,LOW);
    digitalWrite(radioChannel3,LOW);
    pwmWrite(chairCommand,minROT);
    pwmWrite(mirrorCommand,minROT);
    sleep(1);
    pwmWrite(chairCommand,0);
    pwmWrite(mirrorCommand,0);
    std::cout << "Closing program.." << std::endl;
    exit(signum);
}