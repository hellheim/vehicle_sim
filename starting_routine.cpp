#include "header/starting_routine.hpp"

void starting_routine()
{
    digitalWrite(radioChannel0,HIGH);
    digitalWrite(radioChannel1,LOW);
    digitalWrite(radioChannel2,LOW);
    digitalWrite(radioChannel3,LOW);
    usleep(200);
    digitalWrite(radioChannel0,LOW);
    digitalWrite(radioChannel1,HIGH);
    digitalWrite(radioChannel2,LOW);
    digitalWrite(radioChannel3,LOW);
    usleep(200);
    digitalWrite(radioChannel0,LOW);
    digitalWrite(radioChannel1,LOW);
    digitalWrite(radioChannel2,HIGH);
    digitalWrite(radioChannel3,LOW);
    usleep(200);
    digitalWrite(radioChannel0,LOW);
    digitalWrite(radioChannel1,LOW);
    digitalWrite(radioChannel2,LOW);
    digitalWrite(radioChannel3,HIGH);
    usleep(200);
    digitalWrite(radioChannel0,LOW);
    digitalWrite(radioChannel1,LOW);
    digitalWrite(radioChannel2,LOW);
    digitalWrite(radioChannel3,LOW);
    usleep(200);
}