#ifndef SIG_HANDLER_HPP
#define SIG_HANDLER_HPP

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <netdb.h> 
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <wiringPi.h>

#include "error_.hpp"
#include "pinout_param.hpp"

void sig_handler(int signum);

#endif