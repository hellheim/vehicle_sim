#ifndef RADIO_HANDLER_HPP
#define RADIO_HANDLER_HPP

#include "pinout_param.hpp"
#include "write_can_frame.hpp"
#include "parse_entry.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <string>
#include <thread>
#include <signal.h>
#include <wiringPi.h>
#include <errno.h>

void radio_handler(void);

#endif