#ifndef MIRROR_DOWN_HPP
#define MIRROR_DOWN_HPP

#include "pinout_param.hpp"
#include "parse_entry.hpp"
#include <string>
#include "write_can_frame.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <thread>
#include <signal.h>
#include <wiringPi.h>
#include <errno.h>

void mirror_down(void);

#endif