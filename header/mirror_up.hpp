#ifndef MIRROR_UP_HPP
#define MIRROR_UP_HPP

#include "pinout_param.hpp"
#include "write_can_frame.hpp"
#include "parse_entry.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <thread>
#include <signal.h>
#include <wiringPi.h>
#include <errno.h>

void mirror_up(void);

#endif