#ifndef UPDATE_PARAM_VALUE_HPP
#define UPDATE_PARAM_VALUE_HPP

#include <string>
#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include "param_struct.hpp"
#include "get_param_index.hpp"

/**
 * this function updates a given parameter by its name to a new value
 * */

void update_param_value(std::vector<param_struct> *parameters, char * new_value, std::string name, int nbr_param);

#endif 