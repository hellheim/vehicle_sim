#ifndef PINOUT_PARAM_HPP
#define PINOUT_PARAM_HPP

#define chairSwitcherUp  2
#define chairSwitcherDown  3

#define mirrorSwitcherUp  4
#define mirrorSwitcherDown  5

#define radioChannelInc  21
#define radioChannel0  22
#define radioChannel1  23
#define radioChannel2  27
#define radioChannel3  25

#define maxROT 130
#define minROT 30

#define mirrorAdd  20
#define chairAdd  20

#define chairCommand  24
#define mirrorCommand  1



#endif