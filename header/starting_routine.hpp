#ifndef STARTING_ROUTINE_HPP
#define STARTING_ROUTINE_HPP

#include "pinout_param.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <thread>
#include <signal.h>
#include <wiringPi.h>
#include <errno.h>

void starting_routine();

#endif