#ifndef GET_PARAM_VALUE_HPP
#define GET_PARAM_VALUE_HPP

#include <string>
#include <iostream>
#include <fstream>
#include <string.h>
#include "param_struct.hpp"
#include "parse_entry.hpp"

/**
 * this function get's a param_struct and returns it's value 
 * used for debug purpose
 * maybe other purpose
 * */

void get_param_value(param_struct parameter);

#endif 