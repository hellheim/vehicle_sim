#include "header/radio_handler.hpp"

extern int i, fd;
extern bool radio_;

void radio_handler(void)
{
    
    param_struct param;
    std::string entry;
    int Inc = 0;
    if(radio_)
    {
        radio_ = false;
        i++;
        Inc = i%4;
        if (Inc == 0)
        {
            printf("mosaiqueFM on - shemsFM off - RTCI off - ifm off\n");
            digitalWrite(radioChannel0,HIGH);
            digitalWrite(radioChannel1,LOW);
            digitalWrite(radioChannel2,LOW);
            digitalWrite(radioChannel3,LOW);
        }
        else if (Inc == 1)
        {
            printf("mosaiqueFM off - shemsFM on - RTCI off - ifm off\n");
            digitalWrite(radioChannel0,LOW);
            digitalWrite(radioChannel1,HIGH);
            digitalWrite(radioChannel2,LOW);
            digitalWrite(radioChannel3,LOW);
        }
        else if (Inc == 2)
        {
            printf("mosaiqueFM off - shemsFM off - RTCI on - ifm off\n");
            digitalWrite(radioChannel0,LOW);
            digitalWrite(radioChannel1,LOW);
            digitalWrite(radioChannel2,HIGH);
            digitalWrite(radioChannel3,LOW);
        }
        else if (Inc == 3)
        {
            printf("mosaiqueFM off - shemsFM off - RTCI off - ifm on\n");
            digitalWrite(radioChannel0,LOW);
            digitalWrite(radioChannel1,LOW);
            digitalWrite(radioChannel2,LOW);
            digitalWrite(radioChannel3,HIGH);
        }
        entry = "0:param1:"+std::to_string(Inc);
        parse_entry(entry,&param);
        write_can_frame(fd,&param,OPC_READ);
        sleep(1);
        radio_ = true;
    }
}