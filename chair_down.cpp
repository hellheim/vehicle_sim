#include "header/chair_down.hpp"

extern int chairDC, fd;

void chair_down(void)
{
    param_struct param;
    std::string entry;
    chairDC -= chairAdd;
    if(chairDC < minROT)
        chairDC = minROT;
    pwmWrite(chairCommand,chairDC);
    sleep(1);
    pwmWrite(chairCommand,0);
    entry = "0:param2:"+std::to_string(chairDC);
    parse_entry(entry,&param);
    write_can_frame(fd,&param,OPC_READ);
}