#include <iostream>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <thread>
#include <signal.h>
#include <wiringPi.h>
#include <errno.h>

#include "header/get_param_value.hpp"
#include "header/update_param_value.hpp"
#include "header/error_.hpp"
#include "header/config_can.hpp"
#include "header/param_update_listner.hpp"
#include "header/read_can_frame.hpp"
#include "header/write_can_frame.hpp"
#include "header/pinout_param.hpp"
#include "header/radio_handler.hpp"
#include "header/sig_handler.hpp"
#include "header/chair_up.hpp"
#include "header/chair_down.hpp"
#include "header/mirror_up.hpp"
#include "header/mirror_down.hpp"
#include "header/starting_routine.hpp"

using namespace std;

int i = 0;
int chairDC = minROT;
int mirrorDC = minROT;
int fd = 0;
bool radio_ = true;

int main(int argc, char **argv)
{
    signal(SIGINT,sig_handler);
    config_struct_can config_can_;
    char can_int[] = "can0";
    param_struct param;
    vector<param_struct> parameters;
    int opc, tmp;
    config_can(&config_can_, &fd,can_int,RAW_MODE);
    wiringPiSetup();
    //radio setup
    pinMode(radioChannelInc,INPUT);
    pinMode(radioChannel0,OUTPUT);
    pinMode(radioChannel1,OUTPUT);
    pinMode(radioChannel2,OUTPUT);
    pinMode(radioChannel3,OUTPUT);
    //chair setup
    pinMode(chairCommand,PWM_OUTPUT);
    pinMode(chairSwitcherUp,INPUT);
    pullUpDnControl(chairSwitcherUp,PUD_UP);
    pinMode(chairSwitcherDown,INPUT);
    pullUpDnControl(chairSwitcherDown,PUD_UP);
    //mirror setup
    pinMode(mirrorCommand,PWM_OUTPUT);
    pinMode(mirrorSwitcherUp,INPUT);
    pullUpDnControl(mirrorSwitcherUp,PUD_UP);
    pinMode(mirrorSwitcherDown,INPUT);
    pullUpDnControl(mirrorSwitcherDown,PUD_UP);
    //pwm
    pwmSetMode(PWM_MODE_MS);
    pwmSetClock(384);
    pwmSetRange(1024);
    pwmWrite(chairCommand,chairDC);
    pwmWrite(chairCommand,0);
    pwmWrite(mirrorCommand,mirrorDC);
    pwmWrite(mirrorCommand,0);
    //int
    wiringPiISR(radioChannelInc,INT_EDGE_FALLING,&radio_handler);
    wiringPiISR(chairSwitcherDown,INT_EDGE_FALLING,&chair_down);
    wiringPiISR(chairSwitcherUp,INT_EDGE_FALLING,&chair_up);
    wiringPiISR(mirrorSwitcherDown,INT_EDGE_FALLING,&mirror_down);
    wiringPiISR(mirrorSwitcherUp,INT_EDGE_FALLING,&mirror_up);
    starting_routine();
    while(true)
    {
        read_can_frame(fd,&param,&opc);
        if(opc = OPC_UPDATE)
        {
            if(param.name == "param1")
            {
                tmp = *((int *)param.value);
                if (tmp == 0)
                {
                    printf("mosaiqueFM on - shemsFM off - RTCI off - ifm off\n");
                    digitalWrite(radioChannel0,HIGH);
                    digitalWrite(radioChannel1,LOW);
                    digitalWrite(radioChannel2,LOW);
                    digitalWrite(radioChannel3,LOW);
                }
                else if (tmp == 1)
                {
                    printf("mosaiqueFM off - shemsFM on - RTCI off - ifm off\n");
                    digitalWrite(radioChannel0,LOW);
                    digitalWrite(radioChannel1,HIGH);
                    digitalWrite(radioChannel2,LOW);
                    digitalWrite(radioChannel3,LOW);
                }
                else if (tmp == 2)
                {
                    printf("mosaiqueFM off - shemsFM off - RTCI on - ifm off\n");
                    digitalWrite(radioChannel0,LOW);
                    digitalWrite(radioChannel1,LOW);
                    digitalWrite(radioChannel2,HIGH);
                    digitalWrite(radioChannel3,LOW);
                }
                else if (tmp == 3)
                {
                    printf("mosaiqueFM off - shemsFM off - RTCI off - ifm on\n");
                    digitalWrite(radioChannel0,LOW);
                    digitalWrite(radioChannel1,LOW);
                    digitalWrite(radioChannel2,LOW);
                    digitalWrite(radioChannel3,HIGH);
                }
            }
            else if(param.name == "param2")
            {
                tmp = *((int *)param.value);
                chairDC = tmp;
                pwmWrite(chairCommand,tmp);
                sleep(1);
                pwmWrite(chairCommand,0);
            }
            else if(param.name == "param3")
            {
                tmp = *((int *)param.value);
                mirrorDC = tmp;
                pwmWrite(mirrorCommand,tmp);
                sleep(1);
                pwmWrite(mirrorCommand,0);
            }
        }
    }
    digitalWrite(radioChannel0,LOW);
    digitalWrite(radioChannel1,LOW);
    digitalWrite(radioChannel2,LOW);
    digitalWrite(radioChannel3,LOW);
    pwmWrite(chairCommand,minROT);
    pwmWrite(mirrorCommand,minROT);
    sleep(1);
    pwmWrite(chairCommand,0);
    pwmWrite(mirrorCommand,0);
    std::cout << "Closing program.." << std::endl;
    return 0;
}